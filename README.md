![App Screenshot](https://raw.githubusercontent.com/NageshMandal/NageshMandal/main/image%201.png)

# Contact List Generator
CRUD MVC App Using HTML - CSS - JavaScript and IndexedDB API

CRUD operation in MVC is the basic operations, where 
CRUD denotes create, read, update, and delete. But before 
understanding the basic operations of MVC, first, learn about
 MVC. MVC is the Model View Controller. MVC is a design pattern 
 that is used to differentiate the data from business logic and 
 presentation logic.

 ### CONTACT DATA
 ![App Screenshot](https://raw.githubusercontent.com/NageshMandal/NageshMandal/main/image2.png)

### Diagram And Event Flow
![App Screenshot](https://raw.githubusercontent.com/NageshMandal/NageshMandal/main/Picsart_22-10-20_19-46-10-890.jpg)

### Features
1) you can save your contact list in your local browser
2) also update and delete the contacts
### How to test it

1) Clone the repo
2) Run it