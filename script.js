const form = document.querySelector("form");
const submit = document.querySelector(".submit");
const updates = document.querySelector(".update");
const tbody = document.querySelector("table>tbody");


submit.addEventListener('click',()=>{
    let idb = indexedDB.open('curd',1)
    idb.onupgradeneeded=()=>{
    let res = idb.result;
    res.createObjectStore('data',{autoIncrement:true})
    }
    idb.onsuccess=()=>{
    let res = idb.result;
    let tx = res.transaction('data','readwrite')
    let store = tx.objectStore('data')
    store.put({
        name:form[0].value,
        phone:form[1].value,
    })
    }
    location.reload()
})

function read() {
    let idb = indexedDB.open('curd', 1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('data', 'readonly')
        let store = tx.objectStore('data')
        let cursor = store.openCursor()
        cursor.onsuccess = () => {
            let curRes = cursor.result;
            if (curRes) {
                console.log(curRes.value.name);
                tbody.innerHTML += `
                <tr>
                <td>${curRes.value.name}</td>
                <td>${curRes.value.phone}</td>
                <td class="btn" onclick="update(${curRes.key})">Update</td>
                <td class="btn" onclick="del(${curRes.key})">Delete</td>
                </tr>
                `;

                curRes.continue()
            }
        }
    }
}

function del(e) {
    let idb = indexedDB.open('curd',1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('data','readwrite')
        let store = tx.objectStore('data')
        store.delete(e)
        location.reload()
    }
}

let updateKey;

function update(e) {
    submit.style.display = "none";
    updates.style.display = "block";
    updateKey = e;
}
updates.addEventListener('click',()=>{
    let idb = indexedDB.open('curd',1)
    idb.onsuccess = () => {
        let res = idb.result;
        let tx = res.transaction('data','readwrite')
        let store = tx.objectStore('data')
        store.put({
            name:form[0].value,
            phone:form[1].value,
        },updateKey);
        location.reload()
    }

})





read()